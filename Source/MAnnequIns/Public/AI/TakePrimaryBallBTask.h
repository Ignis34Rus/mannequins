// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "TakePrimaryBallBTask.generated.h"

/** */
UCLASS()
class MANNEQUINS_API UTakePrimaryBallBTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	UTakePrimaryBallBTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector PrimaryBallKey;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector OwnBallKey;

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
