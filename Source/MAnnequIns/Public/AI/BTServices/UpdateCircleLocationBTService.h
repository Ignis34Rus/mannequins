// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "UpdateCircleLocationBTService.generated.h"

/** */
UCLASS()
class MANNEQUINS_API UUpdateCircleLocationBTService : public UBTService
{
	GENERATED_BODY()
public:
	/** Default constructor. */
	UUpdateCircleLocationBTService(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector PrimaryEnemyKey;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector CircleIndexKey;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector CircleRadiusKey;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector CircleLocationKey;

public:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
