// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "StunPrimaryEnemyBTask.generated.h"

/** */
UCLASS()
class MANNEQUINS_API UStunPrimaryEnemyBTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	UStunPrimaryEnemyBTask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector PrimaryEnemyKey;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FBlackboardKeySelector PrimaryEnemyStunnedKey;

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
