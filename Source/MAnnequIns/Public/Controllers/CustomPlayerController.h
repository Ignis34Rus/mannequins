// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CustomPlayerController.generated.h"

class UHUDWidget;

/**
 * 
 */
UCLASS()
class MANNEQUINS_API ACustomPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	ACustomPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Widgets", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UHUDWidget> HUDWidgetClass;

	UPROPERTY()
	UHUDWidget* HUDWidget;

public:
	virtual void SetPawn(APawn* InPawn) override;

protected:
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay() override;

private:
	UFUNCTION()
	void OnThrowableBallSelectionChanged(class AThrowableBall* Ball);

};
