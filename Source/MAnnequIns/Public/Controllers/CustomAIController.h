// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CustomAIController.generated.h"

class AActor;
class AThrowableBall;
class ACustomPlayerCharacter;

/** */
UCLASS()
class MANNEQUINS_API ACustomAIController : public AAIController
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	ACustomAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<ACustomAIController*> GroupControllers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float CapsuleRadiusMultipler;

public:
	/**  */
	void GetGroupActors(TArray<AActor*>& OutActors);
	
	/**  */
	UFUNCTION(BlueprintCallable)
	int32 GetGroupActorsNum();

	/**  */
	UFUNCTION(BlueprintCallable)
	int32 GetSelectedCircleIndex() const;

	/**  */
	UFUNCTION(BlueprintCallable)
	bool IsAtCircle() const;

protected:
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
};
