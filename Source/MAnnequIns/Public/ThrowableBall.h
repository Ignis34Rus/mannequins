// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ThrowableBall.generated.h"

class UStaticMeshComponent;

UCLASS()
class MANNEQUINS_API AThrowableBall : public AActor
{
	GENERATED_BODY()
	
public:
	/** Default constructor. */
	AThrowableBall(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Visual, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

public:
	/**  */
	FORCEINLINE UStaticMeshComponent* GetMesh() const { return MeshComponent; }

};
