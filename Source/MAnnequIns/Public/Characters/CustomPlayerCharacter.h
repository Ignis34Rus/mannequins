// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomCharacter.h"
#include "CustomPlayerCharacter.generated.h"

class AThrowableBall;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FThrowableBallSelectionChanged, AThrowableBall*, Ball);

UCLASS()
class MANNEQUINS_API ACustomPlayerCharacter : public ACustomCharacter
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	ACustomPlayerCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/**  */
	UPROPERTY(BlueprintAssignable)
	FThrowableBallSelectionChanged OnThrowableBallSelectionChanged;

private:
	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float ThrowStrength;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float InteractionRadius;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	/**  */
	TWeakObjectPtr<AThrowableBall> SelectedBall;

public:	
	UFUNCTION(BlueprintCallable)
	bool TryGetBall(AThrowableBall*& Actor);

	void SelectBall(AThrowableBall* Ball);

	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
