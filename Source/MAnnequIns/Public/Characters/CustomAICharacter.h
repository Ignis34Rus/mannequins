// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomCharacter.h"
#include "CustomAICharacter.generated.h"

UCLASS()
class MANNEQUINS_API ACustomAICharacter : public ACustomCharacter
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	ACustomAICharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

};
