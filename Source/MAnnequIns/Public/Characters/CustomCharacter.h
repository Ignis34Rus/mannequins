// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CustomCharacter.generated.h"

class AThrowableBall;

UCLASS(Abstract)
class MANNEQUINS_API ACustomCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	ACustomCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
	/**  */
	UPROPERTY(BlueprintReadOnly, Category = BallManagment, meta = (AllowPrivateAccess = "true"))
	AThrowableBall* OwnedBall;

private:
	/**  */
	UPROPERTY(BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	bool IsSprinting;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	float SprintingExtraSpeed;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	float SprintingStaminaCost;

	/**  */
	UPROPERTY(BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	float CurrentStamina;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	float MaximumStamina;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes, meta = (AllowPrivateAccess = "true"))
	float StaminaRegeneration;

public:
	UFUNCTION(BlueprintCallable, Category = BallManagment)
	bool HasBall() const { return OwnedBall != nullptr; }

	UFUNCTION(BlueprintCallable, Category = BallManagment)
	void PlaceBall(const FVector& position);

	UFUNCTION(BlueprintCallable, Category = BallManagment)
	void ThrowBall(const FVector& direction, float strength);

	UFUNCTION(BlueprintCallable, Category = BallManagment)
	void TakeBall(AThrowableBall* ball);

	UFUNCTION(BlueprintCallable, Category = Movement)
	void SetStunned(bool NewValue);

	UFUNCTION(BlueprintCallable, Category = Movement)
	void SetSprinting(bool NewValue);

	virtual void Tick(float DeltaSeconds) override;

protected:
	/** Overridable native event for when play begins for this actor. */
	virtual void BeginPlay() override;

};
