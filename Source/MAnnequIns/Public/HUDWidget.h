// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Slate/SObjectWidget.h"
#include "HUDWidget.generated.h"

class UInventorySystemComponent;

/**  */
UCLASS()
class MANNEQUINS_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	bool IsInteracting;

};
