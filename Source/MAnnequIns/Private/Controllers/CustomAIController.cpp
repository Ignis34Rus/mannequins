// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomAIController.h"
#include "ThrowableBall.h"
#include "CustomPlayerCharacter.h"
#include "EngineUtils.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Navigation/CrowdFollowingComponent.h"

ACustomAIController::ACustomAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>("PathFollowingComponent"))
{
}

void ACustomAIController::GetGroupActors(TArray<AActor*>& OutActors)
{
	for (auto Controller : GroupControllers)
	{
		OutActors.Add(Controller->GetPawn());
	}
}

int32 ACustomAIController::GetGroupActorsNum()
{
	return GroupControllers.Num();
}

int32 ACustomAIController::GetSelectedCircleIndex() const
{
	return GetBlackboardComponent()->GetValueAsInt("CircleIndex");
}

bool ACustomAIController::IsAtCircle() const
{
	return GetBlackboardComponent()->GetValueAsBool("AtCircle");
}

void ACustomAIController::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<ACustomAIController> ControllerItr(GetWorld()); ControllerItr; ++ControllerItr)
	{
		if (*ControllerItr != this)
		{
			GroupControllers.Add(*ControllerItr);
		}
	}

	for (TActorIterator<AThrowableBall> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		Blackboard->SetValueAsObject("PrimaryBall", *ActorItr);
		break;
	}

	for (TActorIterator<ACustomPlayerCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		Blackboard->SetValueAsObject("PrimaryEnemy", *ActorItr);
		break;
	}

	auto CapsuleRadius = GetCharacter()->GetCapsuleComponent()->GetScaledCapsuleRadius() * CapsuleRadiusMultipler;

	Blackboard->SetValueAsFloat("CircleRadius", ((CapsuleRadius * GroupControllers.Num()) / (PI * 2.f)));
}

void ACustomAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	auto CircleRadius = Blackboard->GetValueAsFloat("CircleRadius");
	auto PrimaryBall = Cast<AActor>(Blackboard->GetValueAsObject("PrimaryBall"));
	auto PrimaryBallLocation = PrimaryBall->GetActorLocation();
	auto PrimaryEnemy = Cast<AActor>(Blackboard->GetValueAsObject("PrimaryEnemy"));
	auto PrimaryEnemyLocation = PrimaryEnemy->GetActorLocation();

	auto PrimaryBallLocation2D = FVector2D(PrimaryBallLocation.X, PrimaryBallLocation.Y);
	auto PrimaryEnemyLocation2D = FVector2D(PrimaryEnemyLocation.X, PrimaryEnemyLocation.Y);

	Blackboard->SetValueAsBool(
		"BallInCircle",
		FVector2D::DistSquared(PrimaryBallLocation2D, PrimaryEnemyLocation2D) <= CircleRadius * CircleRadius);
}
