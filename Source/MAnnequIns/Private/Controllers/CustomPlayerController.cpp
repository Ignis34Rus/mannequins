// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomPlayerController.h"
#include "CustomPlayerCharacter.h"
#include "HUDWidget.h"

ACustomPlayerController::ACustomPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void ACustomPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (auto CustomPlayerCharacter = Cast<ACustomPlayerCharacter>(InPawn))
	{
		CustomPlayerCharacter->OnThrowableBallSelectionChanged.RemoveAll(this);
		CustomPlayerCharacter->OnThrowableBallSelectionChanged.AddDynamic(this, &ACustomPlayerController::OnThrowableBallSelectionChanged);
	}
}

void ACustomPlayerController::BeginPlay()
{
	if (!HUDWidget)
	{
		if (!HUDWidgetClass)
		{
			return;
		}

		HUDWidget = NewObject<UHUDWidget>(this, HUDWidgetClass);
		HUDWidget->SetOwningPlayer(this);
		HUDWidget->AddToViewport();
	}
}

void ACustomPlayerController::OnThrowableBallSelectionChanged(AThrowableBall* Ball)
{
	if (HUDWidget)
	{
		HUDWidget->IsInteracting = Ball != nullptr;
	}
}
