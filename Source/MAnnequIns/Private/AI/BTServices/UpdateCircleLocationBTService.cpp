// Fill out your copyright notice in the Description page of Project Settings.


#include "UpdateCircleLocationBTService.h"
#include "CustomCharacter.h"
#include "ThrowableBall.h"
#include "CustomAIController.h"
#include "AIController.h"
#include "Components/CapsuleComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"


UUpdateCircleLocationBTService::UUpdateCircleLocationBTService(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Update Circle Location Service";
	bNotifyBecomeRelevant = true;
	
	PrimaryEnemyKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UUpdateCircleLocationBTService, PrimaryEnemyKey), ACustomCharacter::StaticClass());
	CircleLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UUpdateCircleLocationBTService, CircleLocationKey));;
	CircleIndexKey.AddIntFilter(this, GET_MEMBER_NAME_CHECKED(UUpdateCircleLocationBTService, CircleIndexKey));
	CircleRadiusKey.AddFloatFilter(this, GET_MEMBER_NAME_CHECKED(UUpdateCircleLocationBTService, CircleRadiusKey));
}

void UUpdateCircleLocationBTService::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);

	auto Controller = Cast<ACustomAIController>(OwnerComp.GetAIOwner());
	auto Character = Cast<ACustomCharacter>(Controller->GetPawn());

	auto Blackboard = OwnerComp.GetBlackboardComponent();

	if (Controller)
	{
		TArray<AActor*> GroupActors;

		auto CurrentIndex = Blackboard->GetValueAsInt(CircleIndexKey.SelectedKeyName);
		auto CircleRadius = Blackboard->GetValueAsFloat(CircleRadiusKey.SelectedKeyName);
		auto PrimaryEnemy = Cast<ACustomCharacter>(Blackboard->GetValueAsObject(PrimaryEnemyKey.SelectedKeyName));
		auto PrimaryEnemyLocation = PrimaryEnemy->GetActorLocation();
		Controller->GetGroupActors(GroupActors);
		auto GroupActorsNum = GroupActors.Num() + 1;

		if (CurrentIndex == 0)
		{
			TArray<int> FreeIndices;

			for (int32 i = GroupActorsNum; i > 0; i--)
				FreeIndices.Add(i);

			for (auto& GroupActor : GroupActors)
			{
				if (auto GroupPawn = Cast<APawn>(GroupActor))
				{
					if (auto PawnController = Cast<ACustomAIController>(GroupPawn->GetController()))
					{
						FreeIndices.Remove(PawnController->GetSelectedCircleIndex());
					}
				}
			}

			CurrentIndex = FreeIndices[FMath::RandRange(0, FreeIndices.Num() - 1)];

			Blackboard->SetValueAsInt(CircleIndexKey.SelectedKeyName, CurrentIndex);
		}

		auto CircleLocation = PrimaryEnemyLocation;

		const auto Angle = (PI * 2.f / GroupActorsNum) * (CurrentIndex - 1);

		CircleLocation.X += FMath::Sin(Angle) * CircleRadius;
		CircleLocation.Y += FMath::Cos(Angle) * CircleRadius;

		Blackboard->SetValueAsVector(CircleLocationKey.SelectedKeyName, CircleLocation);
	}
}
