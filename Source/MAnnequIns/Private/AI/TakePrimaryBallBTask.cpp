// Fill out your copyright notice in the Description page of Project Settings.

#include "TakePrimaryBallBTask.h"
#include "CustomCharacter.h"
#include "ThrowableBall.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

UTakePrimaryBallBTask::UTakePrimaryBallBTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Take Primary Ball Task";

	PrimaryBallKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UTakePrimaryBallBTask, PrimaryBallKey), AThrowableBall::StaticClass());
	OwnBallKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UTakePrimaryBallBTask, OwnBallKey));
}

EBTNodeResult::Type UTakePrimaryBallBTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	auto Character = Cast<ACustomCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	auto Blackboard = OwnerComp.GetBlackboardComponent();

	auto PrimaryBall = Cast<AThrowableBall>(Blackboard->GetValueAsObject(PrimaryBallKey.SelectedKeyName));

	Character->TakeBall(PrimaryBall);

	Blackboard->SetValueAsBool(OwnBallKey.SelectedKeyName, true);

	return EBTNodeResult::Succeeded;
}
