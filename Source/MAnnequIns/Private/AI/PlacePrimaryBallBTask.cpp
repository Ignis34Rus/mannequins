// Fill out your copyright notice in the Description page of Project Settings.

#include "PlacePrimaryBallBTask.h"
#include "CustomCharacter.h"
#include "ThrowableBall.h"
#include "AIController.h"
#include "CustomAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

UPlacePrimaryBallBTask::UPlacePrimaryBallBTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Place Primary Ball Task";
	bNotifyTick = true;

	OwnBallKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, OwnBallKey));
	AtCircleKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, AtCircleKey));
	PrimaryBallKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, PrimaryBallKey), AThrowableBall::StaticClass());
	PrimaryEnemyKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, PrimaryEnemyKey), ACustomCharacter::StaticClass());
	PrimaryEnemyStunnedKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, PrimaryEnemyStunnedKey));
	CircleRadiusKey.AddFloatFilter(this, GET_MEMBER_NAME_CHECKED(UPlacePrimaryBallBTask, CircleRadiusKey));
}

EBTNodeResult::Type UPlacePrimaryBallBTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	auto Character = Cast<ACustomCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	auto Blackboard = OwnerComp.GetBlackboardComponent();

	if (Character->HasBall())
	{
		return EBTNodeResult::InProgress;
	}

	Blackboard->SetValueAsBool(AtCircleKey.SelectedKeyName, true);

	return EBTNodeResult::Succeeded;
}

void UPlacePrimaryBallBTask::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);

	auto Controller = Cast<ACustomAIController>(OwnerComp.GetAIOwner());
	auto Character = Cast<ACustomCharacter>(Controller->GetPawn());

	if (Character->HasBall())
	{
		TArray<AActor*> GroupActors;
		Controller->GetGroupActors(GroupActors);

		auto Blackboard = OwnerComp.GetBlackboardComponent();

		for (auto& GroupActor : GroupActors)
		{
			if (auto GroupPawn = Cast<APawn>(GroupActor))
			{
				if (auto PawnController = Cast<ACustomAIController>(GroupPawn->GetController()))
				{
					if (!PawnController->IsAtCircle())
					{
						return;
					}
				}
			}
		}

		auto CircleRadius = Blackboard->GetValueAsFloat(CircleRadiusKey.SelectedKeyName);
		auto PrimaryEnemy = Cast<ACustomCharacter>(Blackboard->GetValueAsObject(PrimaryEnemyKey.SelectedKeyName));
		auto PlaceLocation = PrimaryEnemy->GetActorLocation() + (PrimaryEnemy->GetActorForwardVector() * CircleRadius * 0.35f);

		Character->PlaceBall(PlaceLocation);
		PrimaryEnemy->SetStunned(false);
		Blackboard->SetValueAsBool(OwnBallKey.SelectedKeyName, false);
		Blackboard->SetValueAsBool(AtCircleKey.SelectedKeyName, true);
		Blackboard->SetValueAsBool(PrimaryEnemyStunnedKey.SelectedKeyName, false);

		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
