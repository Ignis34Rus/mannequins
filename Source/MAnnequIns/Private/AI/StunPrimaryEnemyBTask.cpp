// Fill out your copyright notice in the Description page of Project Settings.

#include "StunPrimaryEnemyBTask.h"
#include "CustomCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

UStunPrimaryEnemyBTask::UStunPrimaryEnemyBTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Stun Primary Enemy Task";

	PrimaryEnemyKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UStunPrimaryEnemyBTask, PrimaryEnemyKey), ACustomCharacter::StaticClass());
	PrimaryEnemyStunnedKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UStunPrimaryEnemyBTask, PrimaryEnemyStunnedKey));
}

EBTNodeResult::Type UStunPrimaryEnemyBTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	auto Blackboard = OwnerComp.GetBlackboardComponent();

	auto PrimaryEnemy = Cast<ACustomCharacter>(Blackboard->GetValueAsObject(PrimaryEnemyKey.SelectedKeyName));

	PrimaryEnemy->SetStunned(true);

	Blackboard->SetValueAsBool(PrimaryEnemyStunnedKey.SelectedKeyName, true);

	return EBTNodeResult::Succeeded;
}
