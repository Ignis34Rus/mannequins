// Fill out your copyright notice in the Description page of Project Settings.


#include "ThrowableBall.h"
#include "Components/StaticMeshComponent.h"

AThrowableBall::AThrowableBall(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");

	RootComponent = MeshComponent;
}
