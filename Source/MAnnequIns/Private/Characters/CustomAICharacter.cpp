// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomAICharacter.h"
#include "CustomAIController.h"

ACustomAICharacter::ACustomAICharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ACustomAIController::StaticClass();
}
