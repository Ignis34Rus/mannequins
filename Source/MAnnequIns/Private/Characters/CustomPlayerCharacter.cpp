// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomPlayerCharacter.h"
#include "ThrowableBall.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"

ACustomPlayerCharacter::ACustomPlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(GetMesh(), "head");
	CameraComponent->bUsePawnControlRotation = true;
}

bool ACustomPlayerCharacter::TryGetBall(AThrowableBall*& Ball)
{
	FHitResult OutHit;
	FVector Start = CameraComponent->GetComponentLocation();
	FVector ForwardVector = CameraComponent->GetForwardVector();
	FVector End = ((ForwardVector * InteractionRadius) + Start);

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECollisionChannel::ECC_Visibility))
	{
		Ball = Cast<AThrowableBall>(OutHit.Actor.Get());
		return Ball != nullptr;
	}

	return false;
}

void ACustomPlayerCharacter::SelectBall(AThrowableBall* Ball)
{
	if (Ball != SelectedBall)
	{
		SelectedBall = Ball;
		OnThrowableBallSelectionChanged.Broadcast(Ball);
	}
}

void ACustomPlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!OwnedBall)
	{
		AThrowableBall* Ball;

		if (TryGetBall(Ball))
		{
			if (Ball != SelectedBall)
			{
				SelectBall(Ball);
			}
		}
		else if (SelectedBall.IsValid())
		{
			SelectBall(nullptr);
		}
	}
}

// Called to bind functionality to input
void ACustomPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	FInputActionBinding ThrowPressed("Throw", IE_Pressed);

	ThrowPressed.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
		{
			auto forward = CameraComponent->GetForwardVector();

			ThrowBall(forward, ThrowStrength);
		});
	PlayerInputComponent->AddActionBinding(ThrowPressed);

	FInputActionBinding InteractPressed("Interact", IE_Pressed);

	InteractPressed.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
		{
			if (SelectedBall.IsValid())
			{
				TakeBall(SelectedBall.Get());
				SelectBall(nullptr);
			}
		});
	PlayerInputComponent->AddActionBinding(InteractPressed);

	FInputActionBinding SprintPressed("Sprint", IE_Pressed);

	SprintPressed.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
		{
			SetSprinting(true);
		});
	PlayerInputComponent->AddActionBinding(SprintPressed);

	FInputActionBinding SprintReleased("Sprint", IE_Released);

	SprintReleased.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
		{
			SetSprinting(false);
		});
	PlayerInputComponent->AddActionBinding(SprintReleased);

	auto& MoveForward = PlayerInputComponent->BindAxis("MoveForward");

	MoveForward.AxisDelegate.GetDelegateForManualSet().BindLambda([this](float value)
		{
			if (value != 0.0f)
			{
				AddMovementInput(GetActorForwardVector(), value);
			}
		});

	auto& MoveRight = PlayerInputComponent->BindAxis("MoveRight");

	MoveRight.AxisDelegate.GetDelegateForManualSet().BindLambda([this](float value)
		{
			if (value != 0.0f)
			{
				AddMovementInput(GetActorRightVector(), value);
			}
		});

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}
