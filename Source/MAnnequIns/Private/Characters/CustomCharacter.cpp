// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomCharacter.h"
#include "ThrowableBall.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ACustomCharacter::ACustomCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void ACustomCharacter::PlaceBall(const FVector& position)
{
	if (OwnedBall)
	{
		auto mesh = OwnedBall->GetMesh();
		OwnedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		OwnedBall->SetActorEnableCollision(true);
		OwnedBall->GetMesh()->SetSimulatePhysics(true);
		OwnedBall->SetActorLocation(position);
		OwnedBall = nullptr;
	}
}

void ACustomCharacter::ThrowBall(const FVector& direction, float strength)
{
	if (OwnedBall)
	{
		auto mesh = OwnedBall->GetMesh();
		OwnedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		OwnedBall->SetActorEnableCollision(true);
		mesh->SetSimulatePhysics(true);
		mesh->AddImpulse(direction * strength);
		OwnedBall = nullptr;
	}
}

void ACustomCharacter::TakeBall(AThrowableBall* ball)
{
	if (OwnedBall)
	{
		OwnedBall->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		OwnedBall->SetActorEnableCollision(true);
		OwnedBall->GetMesh()->SetSimulatePhysics(true);
	}

	OwnedBall = ball;
	OwnedBall->GetMesh()->SetSimulatePhysics(false);
	OwnedBall->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "head");
	OwnedBall->SetActorRelativeLocation(FVector(30.0f, 5.0f, 0.f));
	OwnedBall->SetActorEnableCollision(false);
}

void ACustomCharacter::SetStunned(bool NewValue)
{
	auto movement = GetCharacterMovement();

	if (!NewValue)
	{
		movement->SetMovementMode(EMovementMode::MOVE_Walking);
	}
	else
	{
		movement->DisableMovement();
	}
}

void ACustomCharacter::SetSprinting(bool NewValue)
{
	auto movement = GetCharacterMovement();

	if (NewValue != IsSprinting)
	{
		if (IsSprinting)
		{
			movement->MaxWalkSpeed -= SprintingExtraSpeed;
		}
		else
		{
			movement->MaxWalkSpeed += SprintingExtraSpeed;
		}

		IsSprinting = NewValue;
	}
}

void ACustomCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsSprinting)
	{
		auto movement = GetCharacterMovement();

		if (!movement->Velocity.IsZero())
		{
			auto tickCost = SprintingStaminaCost * DeltaSeconds;

			if (CurrentStamina < tickCost)
			{
				SetSprinting(false);
			}

			CurrentStamina = FMath::Clamp(CurrentStamina - tickCost, .0f, MaximumStamina);

			return;
		}

	}

	if (CurrentStamina < MaximumStamina && StaminaRegeneration != 0)
	{
		CurrentStamina = FMath::Clamp(CurrentStamina + (StaminaRegeneration * DeltaSeconds), .0f, MaximumStamina);
	}
}

void ACustomCharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentStamina = MaximumStamina;
}
